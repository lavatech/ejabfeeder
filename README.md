# ejabfeeder

Feeds ejabberd statistics to influxdb v1.

![A grafana dashboard displaying the fed data](images/examplegrafana.png)

Needs to be run as ejabberd user. Needs python3.7 or higher. Some parts are quite hacky.

## Installation

### InfluxDB (v1) setup

Open up influxdb terminal (with `influx`), and run the following commands:

```
CREATE DATABASE ejabstats
CREATE USER ejabviewer WITH PASSWORD 'passwordgoeshere'
CREATE USER ejabfeeder WITH PASSWORD 'anotherpasswordgoeshere'
GRANT READ ON ejabstats TO ejabviewer
GRANT WRITE ON ejabstats TO ejabfeeder
```

### Getting/placing the repo

As we'll likely want to run this script as the ejabberd user, clone it in a place where it can access it, and set the permissions accordingly. We use `/opt/ejabberd/ejabfeeder`.

```
git clone https://gitlab.com/lavatech/ejabfeeder /opt/ejabberd/ejabfeeder
chmod -R ejabberd:ejabberd /opt/ejabberd/ejabfeeder
```

### Python setup

First of all, ensure that you're running python3.7 or higher. If not, install it.

If python3.7+ is not the default python3 install in your system, please don't set it as the default (unless you know what you're doing), instead, either edit `app.py` or create a dedicated script to run the `app.py` file with the right command and hashbang, and edit `ejabfeeder.service` to use that.

Install dependencies with `python3.x -m pip install -Ur requirements.txt`.

### Configuration

Copy `config-example.py` to `config.py`.

Fill in variables prefixed with `influx_` with... you guessed it, your influxdb details.

For `ejabberd_vhost`, place your vhost's name, such as `a3.pm`. Please be aware that not all measurements utilize this value.

### Systemd service and timer setup

(Relevant files are under `systemd-services`)

If you use non-default paths (or users) or if your python3 isn't aliased to 3.7+, go and edit relevant portions of `ejabfeeder.service` first.

Copy both files under `systemd-services` to `/etc/systemd/system/`.

First try running the service once with `systemctl start ejabfeeder`, ensure that it runs with `systemctl status ejabfeeder` and `journalctl -u ejabfeeder`. You may optionally also check influxdb.

Then, once you verify that everything is functional, run `systemctl enable --now ejabfeeder.timer`.

### Grafana setup

An example grafana export is provided under the grafana folder.
