#!/usr/bin/python3
import subprocess
from influxdb import InfluxDBClient
import config

client = InfluxDBClient(
    config.influx_host,
    config.influx_port,
    config.influx_username,
    config.influx_password,
    config.influx_database,
    ssl=config.influx_ssl,
)


influx_tofeed = []


def ejabberd_call(request):
    # TODO: use api instead
    # https://docs.ejabberd.im/developer/ejabberd-api/admin-api/#connected-users etc
    cmd = f"ejabberdctl {request}"
    result = subprocess.run(cmd, shell=True, capture_output=True, check=True)

    return result.stdout.decode()


ejabberd_measurements = {
    "muc_online_rooms_number": {"call": "muc_online_rooms global", "count": True},
    "registered_users_number": {
        "call": f"registered_users {config.ejabberd_vhost}",
        "count": True,
    },
    "connected_users_number": {
        "call": "connected_users_number",
        "count": False,
    },
    "incoming_s2s_number": {
        "call": "incoming_s2s_number",
        "count": False,
    },
    "outgoing_s2s_number": {
        "call": "outgoing_s2s_number",
        "count": False,
    },
    "ipv4_users": {
        "call": "connected_users_info | grep ::ffff",  # TODO: This is bad
        "count": True,
    },
    "ipv6_users": {
        "call": "connected_users_info | grep -v ::ffff",  # TODO: This is bad
        "count": True,
    },
}

for measurement, options in ejabberd_measurements.items():
    result = ejabberd_call(options["call"])
    if options["count"]:
        result = result.count("\n")
    if type(result) == str:
        result = int(result.strip())
    data = {"measurement": measurement, "fields": {"value": result}}
    print(repr(data))
    influx_tofeed.append(data)

client.write_points(influx_tofeed)
